package ar.edu.tadp.superobserver

class SuperObserver {

	//TODO: Las referencias a los objetivos en los triggers deberían ser soft, sino el GC nunca se los lleva.
	static triggers = []

	static eventos(unBloque) {
		this.with(unBloque)
	}

	static cuando(unBloque) {
		def configurator = new SuperObserverConfigurator(unBloque)
		configurator
	}

	static agregarTrigger(objetivo, condicion, accion, propiedades) {
		triggers.add([objetivo:objetivo, propiedades:propiedades, condicion:condicion, accion:accion])
	}

	static propiedadCambiada(unObjeto, propiedad) {
		def triggers = triggers.findAll { aplicaTrigger(propiedad, unObjeto, it)}
		
		triggers.each {
			new TriggerExecutor(it, unObjeto).execute()
		}
	}
	
	static aplicaTrigger(propiedad, unObjeto, trigger) {
		(unObjeto ==  trigger.objetivo || unObjeto in trigger.objetivo) &&
			trigger.propiedades.contains(propiedad) &&
				unObjeto.with(trigger.condicion)
	}
	
	static stopEvents(trigger) {
		triggers.remove(trigger)
	}
}
