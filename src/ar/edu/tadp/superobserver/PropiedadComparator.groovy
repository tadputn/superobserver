package ar.edu.tadp.superobserver

class PropiedadComparator {
	def propiedad
	def comparador
	def valor
	
	def getBloque() {
		//El return es necesario: Expresión de sintaxis ambigua tira, sino
		return { this.comparador(delegate."${this.propiedad}", this.valor) }
	}
	
}
