package ar.edu.tadp.superobserver

class TriggerExecutor {
	
	def objetoADelegar
	def triggerEjecutando
	
	TriggerExecutor(trigger, objeto) {
		objetoADelegar = objeto
		triggerEjecutando = trigger
	}
	
	def execute() {
		this.with(triggerEjecutando.accion)
	}
	
	def propertyMissing(String name) {
		objetoADelegar.getProperty(name)
	}
	
	def propertyMissing(String name, value) {
		objetoADelegar.setProperty(name, value)
	}
	
	def methodMissing(String name, args) {
		objetoADelegar.invokeMethod(name, args)
	}
	
	def stopEvents() {
		SuperObserver.stopEvents(triggerEjecutando)
	}
}
