package ar.edu.tadp.superobserver

/**
 * Para hacer que esta clase funcione bien probablemente sea necesario
 * implementar unas cuantas interfaces. Groovy en algunos casos traduce
 * a métodos estáticos ciertas llamadas, como por ejemplo la comparación
 * entre dos objetos con > o <, y dentro de esas llamadas se fija en el tipo
 * del objeto (hace instanceOf). Hackeril.
 * (ver {@link org.codehaus.groovy.runtime.typehandling.DefaultTypeTransformation#compareToWithEqualityCheck(Object, Object, boolean)})
 */
class DeafObject implements Comparable {
	
	def propertyMissing(String name) {
		this
	}
	
	def propertyMissing(String name, value) {
		this
	}
	
	def methodMissing(String name, args) {
		this
	} 

	@Override
	public int compareTo(Object o) {
		return -1;
	}
	
}
