package ar.edu.tadp.superobserver;

class ClosureInspector {
	def properties = []

	def propertyMissing(String aPropertyName) {
		properties.add(aPropertyName)

		new DeafObject()
	}

	static inspectProperties(closure) {
		def inspector = new ClosureInspector()
		
		inspector.with(closure) 
		
		inspector.properties
	}
}
