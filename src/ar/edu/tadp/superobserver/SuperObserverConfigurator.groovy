package ar.edu.tadp.superobserver

class SuperObserverConfigurator {
	
	def condicion
	def objetivo
	def accion
	
	SuperObserverConfigurator(cuandoValue) {
		if(cuandoValue in Closure) {
			condicion = cuandoValue
		} else {
			condicion = new PropiedadComparator(propiedad:cuandoValue)
		}
	}
	
	def es(unBloque) {
		condicion.comparador = unBloque
		this
	}
	
	def que(unValor) {
		condicion.valor = unValor
		condicion = condicion.getBloque()
		this
	}
	
	def en(unObjetivo) {
		objetivo = unObjetivo
		this
	}
	
	def ejecutar(unBloque) {
		accion = unBloque
		configurar()
	}
	
	def configurar() {
		def propiedades = ClosureInspector.inspectProperties(condicion)
		
		agregarSetters(propiedades)
		
		SuperObserver.agregarTrigger(objetivo, condicion, accion, propiedades)
	}
	
	def agregarSetters(conditionProperties) {
		conditionProperties.each {
			agregarSetter(it)
		}
	}
	
	def agregarSetter(unaProperty) {
		objetivo.metaClass."set${unaProperty.capitalize()}" = { value ->
			// delegate."$unaProperty" = value
			// Si no hago esto tira Stack Overflow...
			delegate.@"$unaProperty" = value
			SuperObserver.propiedadCambiada(delegate, unaProperty)
		}
	}	
}
