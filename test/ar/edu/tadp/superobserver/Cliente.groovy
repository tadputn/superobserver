package ar.edu.tadp.superobserver

class Cliente {
	def notificaciones = [:].withDefault { key -> 0 }

	def methodMissing(String methodName, args) {
		if(methodName.startsWith("notificar") && args.length == 0) {
			def notificacion = methodName.replace("notificar", "")
			notificaciones[notificacion] = notificaciones[notificacion] + 1 
		}
	}
	
}
