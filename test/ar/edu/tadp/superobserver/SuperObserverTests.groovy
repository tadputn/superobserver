package ar.edu.tadp.superobserver

import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

class SuperObserverTests {

	def cuentaCorriente
	def cuentaMorosa
	def cliente
	def clienteMoroso
	def articulo

	@BeforeClass
	static void setUpObserver() {
		SuperObserver.eventos {
			def menor = { a, b -> a < b }

			cuando { saldo < 3000 } en CuentaCorriente ejecutar { cliente.notificarOdio() }
			cuando { saldo > 50000 } en CuentaCorriente ejecutar { cliente.notificarAmor() }

			cuando "cantidad" es menor que 10 en Articulo ejecutar { reponerStock() }

			cuando { fechaDeAcceso > new Date() } en CuentaCorriente ejecutar { cliente.notificarHackeo() }
		}
	}

	@Before
	void setUp() {
		cliente = new Cliente()
		clienteMoroso = new Cliente()
		cuentaCorriente = new CuentaCorriente(cliente:cliente)
		articulo = new Articulo()
		cuentaMorosa = new CuentaCorriente(cliente:clienteMoroso)
		
		SuperObserver.eventos {
			cuando { saldo > 0 } en cuentaMorosa ejecutar { cliente.notificarDejarDePerseguir(); stopEvents()}
		}
	}

	@Test
	void testCondicionBloque() {
		cuentaCorriente.saldo = 3500

		assert 0 == cliente.notificaciones["Amor"]
		assert 0 == cliente.notificaciones["Odio"]

		cuentaCorriente.saldo = 1500
		assert 1 == cliente.notificaciones["Odio"]

		cuentaCorriente.saldo = 70000
		assert 1 == cliente.notificaciones["Amor"]
	}

	@Test
	void testCondicionPropiedad() {
		articulo.cantidad = 20

		assert 20 == articulo.cantidad

		articulo.cantidad = 6
		assert Articulo.STOCK_MAXIMO == articulo.cantidad
	}

	@Test
	void testPropiedadesNoRelacionadasNoTriggerean() {
		cuentaCorriente.saldo = 3500

		assert 0 == cliente.notificaciones["Amor"]
		assert 0 == cliente.notificaciones["Odio"]
		assert 0 == cliente.notificaciones["Hackeo"]

		cuentaCorriente.saldo = 1500
		assert 1 == cliente.notificaciones["Odio"]
		assert 0 == cliente.notificaciones["Amor"]
		assert 0 == cliente.notificaciones["Hackeo"]

		cuentaCorriente.fechaDeAcceso = new Date() + 50
		assert 1 == cliente.notificaciones["Odio"]
		assert 0 == cliente.notificaciones["Amor"]
		assert 1 == cliente.notificaciones["Hackeo"]
	}

	@Test
	void testTriggerEnObjeto() {

		assert 0 == clienteMoroso.notificaciones["DejarDePerseguir"]

		cuentaMorosa.saldo = 500
		assert 1 == clienteMoroso.notificaciones["DejarDePerseguir"]

		cuentaCorriente.saldo = 9000
		assert 0 == cliente.notificaciones["DejarDePerseguir"]
	}

	@Test
	void testStopEvents() {

		assert 0 == clienteMoroso.notificaciones["DejarDePerseguir"]

		cuentaMorosa.saldo = 500
		assert 1 == clienteMoroso.notificaciones["DejarDePerseguir"]

		cuentaMorosa.saldo = 6000
		assert 1 == clienteMoroso.notificaciones["DejarDePerseguir"]
	}
}
