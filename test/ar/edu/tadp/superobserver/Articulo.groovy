package ar.edu.tadp.superobserver

class Articulo {
	def cantidad
	
	final static STOCK_MAXIMO = 100
	
	def reponerStock() {
		cantidad = STOCK_MAXIMO
	}
}
